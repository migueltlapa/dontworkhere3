"""dontworkhere URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import django.contrib.auth.urls
from company.views import hello, hello_template,HelloTemplate,CompanyDetailView,HomeTemplateView
from company import views
urlpatterns = [
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^company/', include('company.urls')),
    url(r'^search_ajax/$', views.lista_doctores1),
    url(r'^create_company/$', views.post_company),

    url(r'^hello/$',hello),
    url(r'^hello_class_view/$', HelloTemplate.as_view()),

    url(r'^detail/(?P<pk>[0-9]+)/$', views.CompanyDetailView.as_view(), name="detail"),

    url(r'^base/$', views.BaseView.as_view(), name="base"),
    url(r'^template/$', views.HomeTemplateView.as_view(), name="template"),
    url(r'^list/$', views.CompanyList.as_view(), name="list"),
    url(r'^create/$', views.CompanyCreate.as_view(), name="create"),
    url(r'^update/(?P<pk>[0-9]+)/$', views.CompanyUpdate.as_view(), name="update"),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.CompanyDelete.as_view(), name="delete"),
]

