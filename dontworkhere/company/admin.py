from django.contrib import admin
from .models import Company,Complaint_Category,Complaint,Article,Comment
# Register your models here.
admin.site.register(Company)
admin.site.register(Complaint_Category)
admin.site.register(Complaint)
admin.site.register(Article)
admin.site.register(Comment)