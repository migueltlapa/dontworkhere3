from django.shortcuts import render
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse,Http404
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views import View
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from django.shortcuts import render_to_response
from . import models
import json
import datetime
# Create your views here.


class BaseView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'hello.html', {
                        "name":"hola base view"
                        })

    def post(self, request):
        return render(request, 'index.html', {
                        "greet":"recibi el post"
                        })

    def update(self, request):
        return render(request, 'index.html', {

            "greet": "update"

        })

class HomeTemplateView(TemplateView):

    template_name = "hello.html"
    print("HomeTemasaplate")
    def get_context_data(self, **kwargs):
        context = super(HomeTemplateView, self).get_context_data(**kwargs)
        context["name"] = " HomeTemplateView"
        return context

class CompanyDetailView(DetailView):
    print("DetailView")
    model = models.Complaint

    template_name = 'detail.html'


class CompanyList(ListView):
    model = models.Complaint
    template_name= 'list.html'

    # def get_context_data(self, **kwargs):
    #     context = super(PersonList, self).get_context_data(**kwargs)
    #     context["object_list"] = models.Person.objects.older()
    #     return context

class CompanyCreate(CreateView):
    model = models.Complaint
    fields = '__all__'
    template_name = 'create.html'
    success_url = reverse_lazy('list')



class CompanyDelete(DeleteView):
    model = models.Complaint
    template_name = 'delete_confirm.html'
    success_url = reverse_lazy('list')


#updateview
class CompanyUpdate(UpdateView):
    model = models.Complaint
    fields = '__all__'
    template_name = 'create.html'
    success_url = reverse_lazy('list')


def index(request):
    now=datetime.datetime.now()
    t = get_template('main.html')
    return render(request,'main.html',{'current_date':now})

    #
    # html=t.render(Context({'current_date':now}))
    # return HttpResponse(html)

#Version Hello
def hello(request):
    name="Mike"
    html="<html><body> Hi %s, this seems to have worked!</body></html>"%name
    return HttpResponse(html)

def hello_template(request):
    name='Mike'
    t= get_template('hello.html')
    return render_to_response('hello.html', {'name': name})

class HelloTemplate(TemplateView):
    template_name = 'hello_class.html'

    def get_context_data(self, **kwargs):
        context=super(HelloTemplate,self).get_context_data(**kwargs)
        context['name']='Hi from Class Hello Template'
        return context


def companies(request):
    z=models.Company.objects.all()
    print("Hola")
    print(z[0].name)
    return render_to_response('companies.html',
                              {'table_s':models.Company.objects.all()})

def company_one(request,company_id=1):
    return render_to_response('company.html',
                              {'table':models.Company.objects.get(id=company_id)})


def lista_doctores1(request):
    if request.is_ajax:
        palabra=request.GET.get('term','')

        doctores=models.Company.objects.filter(name__icontains=palabra)

        results=[]
        print("gato")
        for doctor in doctores:
            doctor_json={}
            doctor_json['label']=doctor.name
            doctor_json['value']=doctor.name
            results.append(doctor_json)

        data_json=json.dumps(results)

    else:
        data_json='fail'
    mimetype="application/json"
    return HttpResponse(data_json,mimetype)


def post_company(request):
    name = 'Mike'

    return render_to_response('create_company.html', {'name': name})